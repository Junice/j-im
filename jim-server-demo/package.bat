@echo off
echo =================================
echo J-IM自动化打包脚本启动...
echo =================================
pushd %1 & for %%i in (.) do set curdir=%%~ni
echo 开始构建文件...

cd /d %cd%

echo %cd%

rd /s/q build
md build\lib\

call mvn clean package -Dmaven.test.skip=true
call mvn dependency:copy-dependencies -DoutputDirectory=build\lib

xcopy /s/e/y    target\*.jar build\lib
xcopy /s/e/i/y  target\classes\config build\config
xcopy /s/e/i/y  target\classes\pages build\pages
xcopy /y        startup.bat build\

rd /s/q target


echo build ok...
echo =================================
echo J-IM build ok:%cd%\build
echo =================================